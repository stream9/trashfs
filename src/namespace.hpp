#ifndef TRASHFS_NAMESPACE_HPP
#define TRASHFS_NAMESPACE_HPP

namespace std::filesystem {}
namespace std::ranges {}
namespace stream9::log {}
namespace stream9::strings {}
namespace stream9::json {}
namespace stream9::xdg::trash {}
namespace stream9::linux {}

namespace trashfs {

namespace rng { using namespace std::ranges; }
namespace fs { using namespace std::filesystem; }
namespace log { using namespace stream9::log; }
namespace str { using namespace stream9::strings; }
namespace json { using namespace stream9::json; }
namespace trash { using namespace stream9::xdg::trash; }
namespace lx { using namespace stream9::linux; }

} // namespace trashfs

#endif // TRASHFS_NAMESPACE_HPP
