#ifndef TRASHFS_PATH_HPP
#define TRASHFS_PATH_HPP

#include <algorithm>
#include <utility>
#include <string_view>

#include <stream9/strings.hpp>

namespace trashfs {

inline bool
is_absolute(str::cstring_view const path) noexcept
{
    return str::starts_with(path, "/");
}

inline std::pair<std::string_view, std::string_view>
split_path(std::string_view const p) noexcept
{
    if (p.empty()) return {};

    auto i1 = p.begin();
    assert(*i1 == '/');
    ++i1;

    auto i2 = std::find(i1, p.end(), '/');

    std::string_view trash_name { i1, i2 };

    if (i2 != p.end()) ++i2;
    std::string_view relpath { i2, p.end() };

    return { trash_name, relpath };
}

inline std::string
resolve_path(std::string_view const base, std::string_view const relative)
{
    std::string s { base };

    s.push_back('/');
    s.append(relative);

    return s;
}

} // namespace trashfs

#endif // TRASHFS_PATH_HPP
