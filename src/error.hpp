#ifndef TRASHFS_ERROR_HPP
#define TRASHFS_ERROR_HPP

#include "namespace.hpp"

#include <system_error>
#include <source_location>

#include <stream9/errors.hpp>
#include <stream9/log.hpp>
#include <stream9/partial_error.hpp>

namespace trashfs {

using stream9::errors::error;

enum class errc {
    invalid_argument,
};

std::error_category const& error_category();

inline std::error_code
make_error_code(errc const e)
{
    return { static_cast<int>(e), error_category() };
}

[[noreturn]] inline void
rethrow_error(std::source_location loc = std::source_location::current())
{
    stream9::errors::rethrow_error(error_category(), std::move(loc));
}

inline void
log_error()
{
    stream9::errors::print_internal_error(log::err());
}

} // namespace trashfs

namespace std {

template<>
struct is_error_code_enum<trashfs::errc>
    : true_type {};

} // namespace std

#endif // TRASHFS_ERROR_HPP
