#include "error.hpp"

#include <string>

namespace trashfs {

std::error_category const&
error_category()
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept override
        {
            return "trashfs";
        }

        std::string message(int const ec) const override
        {
            switch (static_cast<errc>(ec)) {
                using enum errc;
                case invalid_argument:
                    return "invalid argument";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

} // namespace trashfs
