#include "init.hpp"

#include "../context.hpp"
#include "../error.hpp"
#include "../namespace.hpp"

#include <stream9/log.hpp>
#include <stream9/json.hpp>

namespace trashfs::oper {

void*
init(struct fuse_conn_info*, struct fuse_config*) noexcept
{
    try {
        log::info() << "init:" << json::format({ });

        return new context;
    }
    catch (...) {
        log_error();
        std::exit(1);
    }
}

} // namespace trashfs::oper
