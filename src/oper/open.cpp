#include "open.hpp"

#include "../context.hpp"
#include "../error.hpp"
#include "../path.hpp"

#include <shared_mutex>

#include <stream9/log.hpp>
#include <stream9/json.hpp>
#include <stream9/xdg/trash.hpp>
#include <stream9/linux/file.hpp>

namespace trashfs::oper {

int
open(char const* const path, struct fuse_file_info* const fi) noexcept
{
    try {
        if (!path || !fi) return -EINVAL;

        log::info() << "open:" << json::format({
            { "path", (path ? path : "") },
            { "flags", (fi ? fi->flags : 0) },
        });

        auto& cxt = get_context();
        std::shared_lock lk { cxt.mutex };

        auto const& [trash_name, relpath] = split_path(path);
        log::info() << "open:" << trash_name << relpath;
        auto const it = cxt.trash_can->find_trash_entry(trash_name);
        if (it == cxt.trash_can->end()) return -ENOENT;

        auto const& info = it->trash_info();
        auto file_path = info.file_path().string();

        if (!relpath.empty()) {
            file_path.push_back('/');
            file_path.append(relpath); //TODO no copy, resolve_path()
        }

        log::info() << "open:" << file_path //TODO can't << std::oct
                    << lx::file_status_flags_to_symbol(fi->flags);
        auto const rc = ::open(file_path.c_str(), fi->flags);
        if (rc == -1) {
            return -errno;
        }
        else {
            log::info() << "open:" << rc;
            fi->fh = static_cast<uint64_t>(rc);
            return 0;
        }
    }
    catch (...) {
        log_error();
        return -ENOENT; //TODO
    }
}

} // namespace trashfs::oper
