#include "readdir.hpp"

#include "../context.hpp"
#include "../path.hpp"

#include <shared_mutex>

#include <sys/types.h> // opendir
#include <dirent.h>

#include <stream9/json.hpp>
#include <stream9/log.hpp>
#include <stream9/strings.hpp>
#include <stream9/xdg/trash.hpp>

namespace trashfs::oper {

static int
readdir_from_real_directory(
    str::cstring_view const path,
    void* const buf,
    fuse_fill_dir_t filler) noexcept
{
    try {
        auto* const dirp = ::opendir(path); //TODO lx::opendir
        if (!dirp) return -errno;

        log::info() << "readdir: start filling from directory";
        while (true) {
            errno = 0;
            auto* const ent = ::readdir(dirp);
            if (errno || ent == nullptr) break;

            log::info() << "readdir:" << ent->d_name;
            filler(buf, ent->d_name, nullptr, 0, {});
        }

        auto const err = errno;
        auto const rc = ::closedir(dirp);
        if (rc == -1) return -errno;

        return err ? -err : 0;
    }
    catch (...) {
        log_error();
        return 0;
    }
}

int
readdir(char const* path,
        void* const buf,
        fuse_fill_dir_t filler,
        off_t off,
        struct fuse_file_info*,
        enum fuse_readdir_flags) noexcept
{
    try {
        if (!path || !buf) return -EINVAL;

        log::info() << "readdir:" << json::format({
            { "path", path },
            { "off", off },
        });

        auto& cxt = get_context();
        auto const& [trash_name, relpath] = split_path(path);

        log::info() << "readdir:" << std::quoted(trash_name) << std::quoted(relpath);

        if (trash_name.empty()) {
            filler(buf, ".", nullptr, 0, {});
            filler(buf, "..", nullptr, 0, {});

            {
                std::shared_lock lk { cxt.mutex };

                log::info() << "readdir: start filling";
                for (auto const& ent: *cxt.trash_can) {
                    log::info() << "readdir:" << ent.name();
                    filler(buf, ent.name(), nullptr, 0, {});
                }
            }
        }
        else {
            auto const it = cxt.trash_can->find_trash_entry(trash_name);
            if (it == cxt.trash_can->end()) return -ENOENT;

            //if (!is_directory(*it)) return -ENOTDIR; //TODO

            auto const& trash_path = it->trash_info().file_path();
            //assert(fs::is_directory(trash_path)); //TODO
            if (!fs::is_directory(trash_path)) return -ENOTDIR;

            if (relpath.empty()) {
                return readdir_from_real_directory(trash_path, buf, filler);
            }
            else {
                auto const& dir_path = resolve_path(trash_path.c_str(), relpath);

                return readdir_from_real_directory(dir_path, buf, filler);
            }
        }

        return 0;
    }
    catch (...) {
        log_error();
        return -ENOENT; //TODO
    }
}

} // namespace trashfs::oper
