#include "readlink.hpp"

#include "../context.hpp"
#include "../path.hpp"

#include <unistd.h>

#include <stream9/strings.hpp>
#include <stream9/log.hpp>
#include <stream9/xdg/trash.hpp>

namespace trashfs::oper {

static int
do_readlink(str::cstring_ptr const path,
            char* const buf,
            size_t const size) noexcept
{
    auto const rc = ::readlink(path, buf, size - 1);
    if (rc == -1) {
        return -errno;
    }
    else {
        buf[rc] = '\0';
        return 0;
    }
}

int
readlink(char const* const path,
         char* const buf,
         std::size_t const size) noexcept
{
    try {
        if (!path || !buf) return -EINVAL;

        auto& cxt = get_context();
        auto const& [trash_name, relpath] = split_path(path);

        log::info() << "readlink:" << std::quoted(trash_name) << std::quoted(relpath);

        auto const it = cxt.trash_can->find_trash_entry(trash_name);
        if (it == cxt.trash_can->end()) return -ENOENT;

        auto const& trash_path = it->trash_info().file_path();

        if (relpath.empty()) {
            return do_readlink(trash_path, buf, size);
        }
        else {
            auto const& link_path = trash_path / relpath;
            return do_readlink(link_path, buf, size);
        }
    }
    catch (...) {
        //return extract_error_code(); //TODO
        return -ENOENT;
    }
}

} // namespace trashfs::oper
