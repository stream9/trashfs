#include "read.hpp"

#include "../error.hpp"

#include <unistd.h>

#include <stream9/log.hpp>
#include <stream9/json.hpp>

namespace trashfs::oper {

int
read(char const* const path,
     char* const buf,
     std::size_t const size,
     off_t const offset,
     struct fuse_file_info* const fi) noexcept
{
    try {
        if (!fi) return -EINVAL;

        auto const fd = static_cast<int>(fi->fh);

        log::info() << "read:" << json::format({
            { "path", path },
            { "size", size },
            { "offset", offset },
            { "fd", fd },
        });

        auto const rc = ::pread(fd, buf, size, offset);
        if (rc == -1) {
            log::dbg() << errno;
            return -errno;
        }
        else {
            log::dbg() << rc;
            return static_cast<int>(rc); //TODO check overflow
        }
    }
    catch (...) {
        log_error();
        return -ENOENT; //TODO
    }
}

} // namespace trashfs::oper
