#ifndef TRASHFS_OPER_READDIR_HPP
#define TRASHFS_OPER_READDIR_HPP

#include "../fuse.hpp"

namespace trashfs::oper {

int
readdir(char const* path,
        void* buf,
        fuse_fill_dir_t,
        off_t off,
        struct fuse_file_info*,
        enum fuse_readdir_flags) noexcept;

} // namespace trashfs::oper

#endif // TRASHFS_OPER_READDIR_HPP
