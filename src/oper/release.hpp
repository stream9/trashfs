#ifndef TRASHFS_OPER_RELEASE_HPP
#define TRASHFS_OPER_RELEASE_HPP

#include "../fuse.hpp"

namespace trashfs::oper {

int release(char const* path, struct fuse_file_info* fi) noexcept;

} // namespace trashfs::oper

#endif // TRASHFS_OPER_RELEASE_HPP
