#ifndef TRASHFS_OPER_OPEN_HPP
#define TRASHFS_OPER_OPEN_HPP

#include "../fuse.hpp"

namespace trashfs::oper {

int open(char const* const path, struct fuse_file_info* const fi) noexcept;

} // namespace trashfs::oper

#endif // TRASHFS_OPER_OPEN_HPP
