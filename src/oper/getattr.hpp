#ifndef TRASHFS_OPER_GETATTR_HPP
#define TRASHFS_OPER_GETATTR_HPP

#include "../fuse.hpp"

namespace trashfs::oper {

int
getattr(char const* path,
        struct stat*,
        struct fuse_file_info*) noexcept;

} // namespace trashfs::oper

#endif // TRASHFS_OPER_GETATTR_HPP
