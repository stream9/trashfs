#include "release.hpp"

#include "../namespace.hpp"

#include <unistd.h>

#include <stream9/log.hpp>
#include <stream9/json.hpp>

namespace trashfs::oper {

int
release(char const* const path, struct fuse_file_info* const fi) noexcept
{
    if (!fi) return -EINVAL;

    log::info() << "release:" << json::format({
        { "path", path },
        { "fh", fi->fh }
    });

    auto const rc = ::close(static_cast<int>(fi->fh));
    if (rc == -1) {
        return -errno;
    }
    else {
        return 0;
    }
}

} // namespace trashfs::oper
