#ifndef TRASHFS_OPER_INIT_HPP
#define TRASHFS_OPER_INIT_HPP

#include "../fuse.hpp"

namespace trashfs::oper {

void* init(struct fuse_conn_info*, struct fuse_config*) noexcept;

} // namespace trashfs::oper

#endif // TRASHFS_OPER_INIT_HPP
