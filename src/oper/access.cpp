#include "access.hpp"

#include "../context.hpp"
#include "../namespace.hpp"
#include "../path.hpp"

#include <shared_mutex>

#include <stream9/log.hpp>
#include <stream9/json.hpp>
#include <stream9/xdg/trash.hpp>

namespace trashfs::oper {

static int
do_access(str::cstring_view const path, int const mask) noexcept
{
    auto const rc = ::access(path, mask);
    if (rc == -1) {
        return -errno;
    }
    else {
        return 0;
    }
}

int
access(char const* const path, int const mask) noexcept
{
    try {
        if (!path) return -EINVAL;

        log::info() << "access:" << json::format({
            { "path", (path ? path : "") },
            { "mask", mask },
        });

        auto& cxt = get_context();
        std::shared_lock lk { cxt.mutex };

        auto const& [trash_name, relpath] = split_path(path);
        log::info() << "access:" << trash_name << relpath;
        auto const it = cxt.trash_can->find_trash_entry(trash_name);
        if (it == cxt.trash_can->end()) return -ENOENT;

        auto const& info = it->trash_info();
        auto file_path = info.file_path().string();

        if (relpath.empty()) {
            return do_access(file_path, mask);
        }
        else {
            auto const& path = resolve_path(file_path, relpath);
            return do_access(path, mask);
        }
    }
    catch (...) {
        log_error();
        return -ENOENT; //TODO
    }
}

} // namespace trashfs::oper
