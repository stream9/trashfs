#ifndef TRASHFS_OPER_DESTROY_HPP
#define TRASHFS_OPER_DESTROY_HPP

namespace trashfs::oper {

void destroy(void* const private_data) noexcept;

} // namespace trashfs::oper

#endif // TRASHFS_OPER_DESTROY_HPP
