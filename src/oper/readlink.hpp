#ifndef TRASHFS_OPER_READLINK_HPP
#define TRASHFS_OPER_READLINK_HPP

#include <cstddef>

namespace trashfs::oper {

int
readlink(char const* path, char* buf, std::size_t size) noexcept;

} // namespace trashfs::oper

#endif // TRASHFS_OPER_READLINK_HPP
