#ifndef TRASHFS_OPER_READ_HPP
#define TRASHFS_OPER_READ_HPP

#include "../fuse.hpp"

#include <cstddef>

namespace trashfs::oper {

int
read(char const* path,
     char* buf,
     std::size_t size,
     off_t offset,
     struct fuse_file_info*) noexcept;

} // namespace trashfs::oper

#endif // TRASHFS_OPER_READ_HPP
