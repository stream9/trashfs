#include "getattr.hpp"

#include "../context.hpp"
#include "../path.hpp"

#include <shared_mutex>

#include <sys/stat.h>

#include <stream9/log.hpp>
#include <stream9/json.hpp>
#include <stream9/strings.hpp>

namespace trashfs::oper {

int
getattr(char const* const path,
        struct stat* const stbuf,
        struct fuse_file_info* const fi) noexcept
{
    try {
        if (!stbuf) return -EINVAL;
        if (!path && !fi) return -EINVAL;

        log::info() << "getattr:" << json::format({
            { "path", (path ? path : "") },
            { "fd", (fi ? fi->fh : 0) },
        });

        str::cstring_view const p { path };

        if (p.empty()) return -ENOENT;
        if (!is_absolute(p)) return -ENOENT;

        if (fi) {
            auto const fd = static_cast<int>(fi->fh);

            auto const rc = ::fstat(fd, stbuf);
            if (rc == -1) return -errno;
        }
        else {
            assert(path);

            auto const& [trash_name, relpath] = split_path(path);
            log::info() << "getattr:" << std::quoted(trash_name) << std::quoted(relpath);

            if (trash_name.empty()) {
                stbuf->st_mode = S_IFDIR | 0755;
                stbuf->st_nlink = 2;
            }
            else {
                auto& cxt = get_context();
                std::shared_lock lk { cxt.mutex };

                auto const it = cxt.trash_can->find_trash_entry(trash_name);
                if (it == cxt.trash_can->end()) return -ENOENT;

                auto const& info = it->trash_info();
                auto const& file_path = info.file_path();

                if (relpath.empty()) {
                    auto const rc = ::lstat(file_path.c_str(), stbuf);
                    if (rc == -1) return -errno;
                }
                else {
                    auto const path = resolve_path(file_path.c_str(), relpath);

                    auto const rc = ::lstat(path.c_str(), stbuf);
                    if (rc == -1) return -errno;
                }
            }
        }

        //info() << "getattr: exit"; //TODO log::scope << timer << function
        return 0;
    }
    catch (...) {
        log_error();
        return -ENOENT; //TODO
    }
}

} // namespace trashfs::oper
