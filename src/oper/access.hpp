#ifndef TRASHFS_OPER_ACCESS_HPP
#define TRASHFS_OPER_ACCESS_HPP

namespace trashfs::oper {

int
access(char const* name, int mask) noexcept;

} // namespace trashfs::oper

#endif // TRASHFS_OPER_ACCESS_HPP
