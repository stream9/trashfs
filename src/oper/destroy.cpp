#include "destroy.hpp"

#include "../context.hpp"
#include "../error.hpp"

#include <errno.h> // eventfd
#include <sys/eventfd.h>

namespace trashfs::oper {

void
destroy(void* const private_data) noexcept
{
    try {
        if (!private_data) return;

        auto const cxt = static_cast<context*>(private_data);
        ::eventfd_write(cxt->stop_event_fd, 1); //TODO lx::eventfd

        delete static_cast<context*>(private_data);
    }
    catch (...) {
        log_error();
    }
}

} // namespace trashfs::oper
