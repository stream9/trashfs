#ifndef TRASHFS_CONTEXT_HPP
#define TRASHFS_CONTEXT_HPP

#include "error.hpp"
#include "namespace.hpp"

#include <memory>
#include <mutex>
#include <shared_mutex>
#include <thread>

#include <errno.h> // eventfd
#include <sys/eventfd.h>

#include <signal.h> // kill

#define FUSE_USE_VERSION 31
#include <fuse.h>

#include <stream9/linux/epoll.hpp>
#include <stream9/log.hpp>
#include <stream9/xdg/trash.hpp>

namespace trashfs {

struct context
{
    trash::linux_environment env;
    std::shared_ptr<trash::trash_can> trash_can;
    std::shared_mutex mutex;
    std::jthread update_thread;
    int stop_event_fd;

    context() try
        : trash_can { trash::trash_can::construct(env) }
    {
        auto const rc = ::eventfd(0, 0); //TODO lx::eventfd
        if (rc == -1) {
            throw "eventfd"; //TODO
        }
        else {
            stop_event_fd = rc;
        }

        update_thread = std::jthread([&](auto token) {
            event_loop(token);
        });
    }
    catch (...) {
        rethrow_error();
    }

    void
    event_loop(std::stop_token token)
    {
        try {
            using namespace std::literals;

            log::dbg() << "event_loop";

            lx::epoll ep;

            ep.add(env.monitor_fd(), EPOLLIN);
            ep.add(stop_event_fd, EPOLLIN);

            bool stop_event = false;
            while (!token.stop_requested() && !stop_event) {
                log::dbg() << "epoll_wait";
                try {
                    for (auto const& e: ep.wait()) {
                        if (e.data.fd == env.monitor_fd()) {
                            std::unique_lock lk { mutex };

                            log::dbg() << "process_event";
                            env.process_events(1s);
                        }
                        else if (e.data.fd == stop_event_fd) {
                            stop_event = true;
                        }
                    }
                }
                catch (error const& e) {
                    if (e.why() == lx::make_error_code(EINTR)) {
                        continue; // epoll_wait got interrupted
                    }
                    else {
                        throw;
                    }
                }
            }
        }
        catch (...) {
            log_error();
            ::kill(0, SIGHUP);
        }
    }
};

inline context&
get_context() noexcept
{
    return *static_cast<context*>(::fuse_get_context()->private_data);
}

} // namespace trashfs

#endif // TRASHFS_CONTEXT_HPP
