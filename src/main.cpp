#include "fuse.hpp"

#include "oper/init.hpp"
#include "oper/destroy.hpp"
#include "oper/open.hpp"
#include "oper/release.hpp"
#include "oper/read.hpp"
#include "oper/getattr.hpp"
#include "oper/readlink.hpp"
#include "oper/readdir.hpp"
#include "oper/access.hpp"

namespace trashfs {

fuse_operations op {
    .getattr = oper::getattr,
    .readlink = oper::readlink,
    .mknod = nullptr,
    .mkdir = nullptr,
    .unlink = nullptr,
    .rmdir = nullptr,
    .symlink = nullptr,
    .rename = nullptr,
    .link = nullptr,
    .chmod = nullptr,
    .chown = nullptr,
    .truncate = nullptr,
    .open = oper::open,
    .read = oper::read,
    .write = nullptr,
    .statfs = nullptr,
    .flush = nullptr,
    .release = oper::release,
    .fsync = nullptr,
    .setxattr = nullptr,
    .getxattr = nullptr,
    .listxattr = nullptr,
    .removexattr = nullptr,
    .opendir = nullptr,
    .readdir = oper::readdir,
    .releasedir = nullptr,
    .fsyncdir = nullptr,
    .init = oper::init,
    .destroy = oper::destroy,
    .access = oper::access,
    .create = nullptr,
    .lock = nullptr,
    .utimens = nullptr,
    .bmap = nullptr,
    .ioctl = nullptr,
    .poll = nullptr,
    .write_buf = nullptr,
    .read_buf = nullptr,
    .flock = nullptr,
    .fallocate = nullptr,
    .copy_file_range = nullptr,
    .lseek = nullptr,
};

} // namespace trashfs

int
main(int argc, char* argv[]) noexcept
{
    using namespace trashfs;

    return ::fuse_main(argc, argv, &op, nullptr);
}
